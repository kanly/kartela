package org.nama.kartela.helper;

import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import org.joda.time.LocalDate;
import org.joda.time.LocalDate.Property;

public final class Dates {
    private Dates() {}

    public static FluentIterable<LocalDate> daysInProperty(Property property) {
        Builder<LocalDate> builder = ImmutableList.builder();
        LocalDate firstDayOfMonth = property.withMinimumValue();
        LocalDate lastDayOfMonth = property.withMaximumValue();
        for (LocalDate dateToAdd = firstDayOfMonth; !dateToAdd.isAfter(lastDayOfMonth); dateToAdd = dateToAdd.plusDays(1)) {
            builder.add(dateToAdd);
        }

        return FluentIterable.from(builder.build());
    }
}
