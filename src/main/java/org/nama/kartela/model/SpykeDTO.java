package org.nama.kartela.model;

import java.io.Serializable;

public class SpykeDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private String name;
    private String surname;
    private int amount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
