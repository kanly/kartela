package org.nama.kartela.service;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.base.MapGlobalResolver;
import org.drools.persistence.jpa.KnowledgeStoreService;
import org.drools.runtime.Environment;
import org.drools.runtime.EnvironmentName;
import org.drools.runtime.StatefulKnowledgeSession;
import org.nama.kartela.model.SpykeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManagerFactory;

@Service
public class SpykeService {

    @Autowired
    @Qualifier("knowledgeStore")
    private KnowledgeStoreService knowledgeStore;

    @Autowired
    @Qualifier("knowledgeBase")
    private KnowledgeBase knowledgeBase;

    @Autowired
    @Qualifier("entityManagerFactory")
    private EntityManagerFactory emf;

    @Autowired
    @Qualifier("transactionManager")
    private JpaTransactionManager transactionManager;

    public void startProcess() {
        StatefulKnowledgeSession kSession = knowledgeStore.newStatefulKnowledgeSession(knowledgeBase, null, getEnvironment());
        kSession.insert(new SpykeDTO());
        kSession.startProcess("org.nama.spyke");
        kSession.fireAllRules();
        kSession.dispose();
    }

    private Environment getEnvironment() {
        Environment environment = KnowledgeBaseFactory.newEnvironment();
        environment.set(EnvironmentName.ENTITY_MANAGER_FACTORY, emf);
        environment.set(EnvironmentName.TRANSACTION_MANAGER, transactionManager);
        environment.set(EnvironmentName.GLOBALS, new MapGlobalResolver());
        return environment;
    }

}
