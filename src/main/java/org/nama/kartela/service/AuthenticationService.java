package org.nama.kartela.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

@Service
public class AuthenticationService {
    private static final Logger log = LoggerFactory.getLogger(AuthenticationService.class);

    public void handleAuthentication(String login, String password, HttpServletRequest httpRequest) {
        log.debug("Authenticating user {}", login);
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(login, password);
        token.setDetails(new WebAuthenticationDetails(httpRequest));

        ServletContext servletContext = httpRequest.getSession().getServletContext();
        WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);

        AuthenticationManager authManager = wac.getBean(AuthenticationManager.class);
        Authentication authentication = authManager.authenticate(token);
        log.debug("User {} authentication completed {}", login, authentication.isAuthenticated() ? "successfully"
            : "unsuccessfully");
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    public void handleLogout(HttpServletRequest httpRequest) {

        ServletContext servletContext = httpRequest.getSession().getServletContext();
        WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);

        LogoutHandler logoutHandler = wac.getBean(LogoutHandler.class);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        logoutHandler.logout(httpRequest, null, authentication);
    }

}
