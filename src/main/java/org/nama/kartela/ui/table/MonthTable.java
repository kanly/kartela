package org.nama.kartela.ui.table;

import com.vaadin.data.Container;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.ui.Table;
import org.nama.kartela.ui.Events;
import org.nama.kartela.ui.bean.Timesheet;
import org.nama.kartela.ui.bean.WorkDay;
import org.nama.kartela.ui.event.WorkDayChangeEvent;

public class MonthTable extends Table {
    private static final long serialVersionUID = 1L;

    public MonthTable(Timesheet timesheet) {
        Container container = createContainer(timesheet);
        setContainerDataSource(container);

    }

    private Container createContainer(Timesheet timesheet) {
        BeanContainer<Long, WorkDay> container = new BeanContainer<Long, WorkDay>(WorkDay.class);
        container.setBeanIdProperty("id");
        for (WorkDay workDay : timesheet.days) {
            container.addBean(workDay);
        }
        container.addItemSetChangeListener(new ItemSetChangeListener() {
            private static final long serialVersionUID = 1L;

            @Override
            public void containerItemSetChange(ItemSetChangeEvent event) {
                Events.post(new WorkDayChangeEvent(event.getContainer()));
            }
        });
        return container;
    }
}
