package org.nama.kartela.ui;

import com.google.common.eventbus.EventBus;

public class Events {

    private static EventBus get() {
        return Application.getCurrent().getEventBus();
    }

    public static void register(Object listener) {
        get().register(listener);
    }

    public static void post(Object event) {
        get().post(event);
    }

}
