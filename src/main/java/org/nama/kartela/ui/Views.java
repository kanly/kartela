package org.nama.kartela.ui;

import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Predicates.not;

import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;
import com.vaadin.navigator.View;
import org.nama.kartela.ui.view.BaseView;
import org.nama.kartela.ui.view.HomeView;
import org.nama.kartela.ui.view.LoginView;
import org.nama.kartela.ui.view.TestDroolsAndBpmView;
import org.nama.kartela.ui.view.TimesheetView;

public enum Views {
    NULL(null, null, null),
    HOME(HomeView.class, "Home", "home"),
    LOGIN(LoginView.class, "Login", "login"),
    TIMESHEET(TimesheetView.class, "Timesheet", "Timesheet"),
    TEST(TestDroolsAndBpmView.class, "Test Stuff", "test");

    public final Class<? extends BaseView<?>> clazz;
    public final String viewName;
    public final String urlStr;

    private Views(Class<? extends BaseView<?>> clazz, String viewName, String urlStr) {
        this.clazz = clazz;
        this.viewName = viewName;
        this.urlStr = urlStr;
    }

    public static Views from(@SuppressWarnings("rawtypes") Class<? extends BaseView> viewType) {
        for (Views view : values()) {
            if (view.clazz == viewType)
                return view;
        }
        return NULL;
    }

    public static Views from(View view) {
        if ((view == null) || !(view instanceof BaseView<?>))
            return NULL;
        return from(((BaseView<?>)view).getClass());
    }

    public static FluentIterable<Views> validViews() {
        return FluentIterable.from(Lists.newArrayList(values())).filter(not(equalTo(NULL)));
    }
}
