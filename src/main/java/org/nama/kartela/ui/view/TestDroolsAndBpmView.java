package org.nama.kartela.ui.view;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import org.nama.kartela.service.SpykeService;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable(autowire = Autowire.BY_TYPE, preConstruction = true)
public class TestDroolsAndBpmView extends BaseView<VerticalLayout> {
    private static final long serialVersionUID = 1L;

    @Autowired
    private SpykeService spykeService;

    public TestDroolsAndBpmView() {
        super(new VerticalLayout());
        Label label = new Label("That's only a test");

        Button button = new Button("Test Them");
        button.addClickListener(new Button.ClickListener() {
            private static final long serialVersionUID = 1L;

            @Override
            public void buttonClick(ClickEvent event) {
                spykeService.startProcess();
                addComponent(new Label("Well Done!"));
            }
        });

        addComponent(label);
        addComponent(button);
    }

    @Override
    protected void onEnter(ViewChangeEvent event) {}

}
