package org.nama.kartela.ui.view;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import org.nama.kartela.ui.Events;
import org.nama.kartela.ui.event.LoginEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoginView extends BaseView<VerticalLayout> implements ClickListener {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(LoginView.class);
    private final PasswordField passwordField = new PasswordField("Password");
    private final TextField usernameField = new TextField("Username");
    private final Button submit = new Button("login", this);

    public LoginView() {
        super(new VerticalLayout());

        addComponent(usernameField);
        addComponent(passwordField);
        addComponent(submit);
    }

    @Override
    public void buttonClick(ClickEvent event) {
        log.trace("submit pressed");
        LoginEvent loginEvent = new LoginEvent(usernameField.getValue(), passwordField.getValue());

        Events.post(loginEvent);

        usernameField.setValue("");
        passwordField.setValue("");
    }

    @Override
    protected void onEnter(ViewChangeEvent event) {}
}
