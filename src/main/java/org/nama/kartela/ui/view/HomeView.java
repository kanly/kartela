package org.nama.kartela.ui.view;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class HomeView extends BaseView<VerticalLayout> {
    private static final long serialVersionUID = 1L;

    public HomeView() {
        super(new VerticalLayout());
        setSizeFull();
        addComponent(new Label("Home"));
    }

    @Override
    protected void onEnter(ViewChangeEvent event) {

    }

}
