package org.nama.kartela.ui.view;

import com.google.common.eventbus.Subscribe;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import org.joda.time.LocalDate;
import org.nama.kartela.controller.TimesheetController;
import org.nama.kartela.ui.Events;
import org.nama.kartela.ui.bean.Timesheet;
import org.nama.kartela.ui.event.WorkDayChangeEvent;
import org.nama.kartela.ui.table.MonthTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable(autowire = Autowire.BY_TYPE, preConstruction = true)
public class TimesheetView extends BaseView<VerticalLayout> {
    private static final long serialVersionUID = 1L;

    private static final Logger log = LoggerFactory.getLogger(TimesheetView.class);

    @Autowired
    private TimesheetController controller;

    public TimesheetView() {
        super(new VerticalLayout());
        Events.register(this);
        buildPage(controller.getTimesheetForMonth(LocalDate.now().monthOfYear()));
    }

    private void buildPage(Timesheet timesheet) {
        addComponent(new Label("Timesheet per il mese: " + timesheet.month.toString("MM/yyyy")));
        MonthTable table = new MonthTable(timesheet);
        addComponent(table);
    }

    @Override
    protected void onEnter(ViewChangeEvent event) {}

    @Subscribe
    public void WorkDaysUpdated(WorkDayChangeEvent event) {
        log.info("Workdays updated");
    }
}
