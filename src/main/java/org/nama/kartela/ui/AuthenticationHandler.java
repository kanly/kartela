package org.nama.kartela.ui;

import com.vaadin.navigator.ViewChangeListener;
import org.nama.kartela.ui.event.UnknownUserEvent;
import org.nama.kartela.ui.view.LoginView;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationHandler implements ViewChangeListener {
    private static final long serialVersionUID = 1L;

    @Override
    public boolean beforeViewChange(ViewChangeEvent event) {
        if (event.getNewView() instanceof LoginView) {
            return true;
        }

        Application currentUI = Application.getCurrent();
        SecurityContext securityContext;
        if (currentUI != null && currentUI.getSecurityContext() != null) {
            securityContext = currentUI.getSecurityContext();
            SecurityContextHolder.setContext(securityContext);
        } else {
            // in this case the context is tipically an empty one
            securityContext = SecurityContextHolder.getContext();
        }
        Authentication authentication = securityContext.getAuthentication();
        boolean isAuthenticated = authentication == null ? false : authentication.isAuthenticated();
        if (!isAuthenticated) {
            Events.post(new UnknownUserEvent());
        }
        return isAuthenticated;
    }

    @Override
    public void afterViewChange(ViewChangeEvent event) {}

}
