package org.nama.kartela.ui;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Layout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.VerticalSplitPanel;
import org.nama.kartela.service.AuthenticationService;
import org.nama.kartela.ui.event.LoginEvent;
import org.nama.kartela.ui.event.LogoutEvent;
import org.nama.kartela.ui.event.UnknownUserEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

@Theme(CustomTheme.THEME_NAME)
@Configurable(autowire = Autowire.BY_TYPE, preConstruction = true)
public class Application extends UI {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(Application.class);
    private final VerticalSplitPanel mainLayout = new VerticalSplitPanel();
    private final Layout headerLayout = new VerticalLayout();
    private final Layout contentLayout = new VerticalLayout();
    private final Navigator navigator = new Navigator(this, contentLayout);
    private final EventBus eventBus = new EventBus();
    private SecurityContext securityContext = null;

    @Autowired
    private AuthenticationHandler authenticationHandler;

    @Autowired
    private AuthenticationService authenticationService;

    @Override
    protected void init(VaadinRequest request) {
        log.info("initing a new UI");
        configureMainLayout();
        buildNavigator();
        addHeader();
        mainLayout.setSecondComponent(contentLayout);
        navigator.navigateTo(Views.LOGIN.urlStr);
        eventBus.register(this);
    }

    @Subscribe
    public void login(LoginEvent event) {
        try {
            authenticate(event.username, event.password);
            navigator.navigateTo(Views.HOME.urlStr);
        } catch (BadCredentialsException e) {
            Notification.show("Bad credentials", Type.ERROR_MESSAGE);
        }
    }

    private void authenticate(String username, String password) {
        authenticationService.handleAuthentication(username, password, ApplicationServlet.currentRequest());
        securityContext = SecurityContextHolder.getContext();
    }

    @Subscribe
    public void logout(LogoutEvent event) {
        authenticationService.handleLogout(ApplicationServlet.currentRequest());
        navigator.navigateTo(Views.LOGIN.urlStr);
    }

    @Subscribe
    public void unknownUser(UnknownUserEvent event) {
        log.error("e questo chi e'??");
        navigator.navigateTo(Views.LOGIN.urlStr);
        Notification.show("Unknown user", Type.ERROR_MESSAGE);
    }

    private void configureMainLayout() {
        mainLayout.setSizeFull();
        mainLayout.setLocked(true);
        mainLayout.addStyleName("main");
        mainLayout.addStyleName(CustomTheme.SPLITPANEL_SMALL);
        mainLayout.setSplitPosition(23, Unit.PIXELS);
        setContent(mainLayout);

    }

    private void addHeader() {
        headerLayout.addComponent(buildMenuBar());
        mainLayout.setFirstComponent(headerLayout);
    }

    private void buildNavigator() {
        for (Views view : Views.validViews()) {
            navigator.addView(view.urlStr, view.clazz);
        }
        navigator.addViewChangeListener(authenticationHandler);
    }

    private MenuBar buildMenuBar() {
        MenuBar menuBar = new MenuBar();
        for (Views view : Views.validViews()) {
            menuBar.addItem(view.viewName, new NavigateTo(view));
        }
        return menuBar;
    }

    public static Application getCurrent() {
        return (Application)UI.getCurrent();
    }

    private class NavigateTo implements MenuBar.Command {
        private static final long serialVersionUID = 1L;
        private final String urlStr;

        public NavigateTo(Views viewType) {
            urlStr = viewType.urlStr;
        }

        @Override
        public void menuSelected(MenuBar.MenuItem selectedItem) {
            navigator.navigateTo(urlStr);
        }
    }

    EventBus getEventBus() {
        return eventBus;
    }

    public SecurityContext getSecurityContext() {
        return securityContext;
    }
}
