package org.nama.kartela.ui;

import com.vaadin.server.VaadinServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ApplicationServlet extends VaadinServlet {
    private static final long serialVersionUID = -4224639491956329857L;
    private static final Logger log = LoggerFactory.getLogger(ApplicationServlet.class);
    private static final ThreadLocal<HttpServletRequest> requestHolder = new ThreadLocal<HttpServletRequest>();

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.trace("Serving request");
        SecurityContextHolder.setContext(SecurityContextHolder.createEmptyContext());
        requestHolder.set(request);
        try {
            super.service(request, response);
        } finally {
            requestHolder.remove();
            SecurityContextHolder.clearContext();
        }
    }

    public static HttpServletRequest currentRequest() {
        return requestHolder.get();
    }

}
