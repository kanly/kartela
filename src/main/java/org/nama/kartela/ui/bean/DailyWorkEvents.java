package org.nama.kartela.ui.bean;

import org.joda.time.Hours;

public class DailyWorkEvents implements ViewBean {
    private static final long serialVersionUID = 1L;
    private EventType type;
    private Hours quantity;
    private Task task;
    private Boolean approved;

    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public Hours getQuantity() {
        return quantity;
    }

    public void setQuantity(Hours quantity) {
        this.quantity = quantity;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

}
