package org.nama.kartela.ui.bean;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import org.joda.time.LocalDate;
import org.joda.time.LocalDate.Property;
import org.nama.kartela.helper.Dates;

public class Timesheet implements ViewBean {
    private static final long serialVersionUID = 1L;

    public final ImmutableList<WorkDay> days;
    public final LocalDate month;

    public Timesheet() {
        // TODO implement! For test only
        month = LocalDate.now();

        Property daysOfMonth = month.dayOfMonth();
        days = Dates.daysInProperty(daysOfMonth).transform(new Function<LocalDate, WorkDay>() {
            private long id = 0;

            @Override
            public WorkDay apply(LocalDate input) {
                WorkDay workDay = new WorkDay();
                workDay.setDay(input);
                workDay.setId(id++);
                return workDay;
            }
        }).toImmutableList();
    }

    public static void main(String[] args) {
        System.out.println(LocalDate.now().dayOfMonth().withMaximumValue().toString());
    }
}
