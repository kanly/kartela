package org.nama.kartela.ui.bean;

import org.joda.time.LocalDate;

import java.util.Set;

public class WorkDay implements ViewBean {
    private static final long serialVersionUID = 1L;
    private Long id;
    private LocalDate day;
    private Set<DailyWorkEvents> events;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDay() {
        return day;
    }

    public void setDay(LocalDate day) {
        this.day = day;
    }

    public Set<DailyWorkEvents> getEvents() {
        return events;
    }

    public void setEvents(Set<DailyWorkEvents> events) {
        this.events = events;
    }

}
