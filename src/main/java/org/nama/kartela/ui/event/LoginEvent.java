package org.nama.kartela.ui.event;

public final class LoginEvent {
    public final String username;
    public final String password;

    public LoginEvent(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
