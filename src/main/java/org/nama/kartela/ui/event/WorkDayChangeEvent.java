package org.nama.kartela.ui.event;

import com.vaadin.data.Container;

public class WorkDayChangeEvent {
    public final Container container;

    public WorkDayChangeEvent(Container container) {
        this.container = container;
    }
}
